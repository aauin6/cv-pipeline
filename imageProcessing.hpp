#pragma once

#include "csvReader.hpp"

#define DEBUG_MODE 0

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <unordered_map>
#include <fstream>

struct frameData;

using namespace cv;

enum class kernelType {
    unset,
    sobel,
    scharr,
    laplacian,
    laplacianWithDiagonals,
    strongScharr
};

enum class blurType {
    unset,
    bilateral,
    gaussian,
    mean,
    median
};

enum class contrastType {
    unset,
    histogramStretching
};

enum class thresholdType {
    unset,
    binary,
    adaptiveMean,
    adaptiveGaussian,
    otsu
};

enum class morphologyType {
    unset,
    open,
    close,
    erode,
    dilate,
    combine,
    extend,
    isolateLines
};

enum class morphologyShape {
    unset,
    rectFrame,
    rect,
    cross
};

enum class orientationType {
    unset,
    vertical,
    horizontal
};

enum class kernelGenerationType {
    unset,
    doubleEdge,
    thickEdge
};

struct objectPixel {
    int row = { -1 },
        col = { -1 };
    int value = { -1 };
};

/**
 * @brief Each vector<objectPixel> represents one window.
 */
struct windowBlobs {
    std::vector<std::vector<objectPixel>> closedWindowBlobs;
    std::vector<std::vector<objectPixel>> openWindowBlobs;
};

/**
 * @brief Renders images side-by-side in a tile layout.
 * @param images - A vector containing all matrices to be displayed
 * @param output - Output image matrix
 * @param cols - Number of columns in the rendered tile layout
 * @param label - Label on the image display window [Optional]
 */
void imageTiling(std::vector<Mat>, Mat&, int = 3, std::string = "Default image tiling title");

/**
 * @brief Applies a bluring filter to an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param bt - Type of blurring effect
 * @param kernelSize - Kernel size of blur filter (Only necessary with GassianCustom blur type)
 * @param required - Is a blur type required? [Optional]
 */
void blurEffect(Mat, Mat&, blurType = blurType::unset, int = 9, bool = false);

/**
 * @brief Applies a edge detection filter to an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param kt - Type of edge detection effect
 * @param bt - Type of blurring effect [Optional]
 * @param kernelSize - Kernel size of blur filter (Only necessary with GassianCustom blur type)
 */
void edgeDetection(Mat, Mat&, kernelType = kernelType::unset, blurType = blurType::unset, int = 9);

/**
 * @brief A edge detection kernel generator for "double" edge detection.
 * @param gap - The gap between each base kernels
 * @param baseKernel - The base edge detection kernel
 * @param dir - Direction of edge detection kernel <vertical or horizontal line scan>
 * @param kgt - Experimental edge detection kernel type
 * @return A "double" edge detection kernel
 */
Mat kernelGenerator(int, Mat, orientationType = orientationType::unset, kernelGenerationType = kernelGenerationType::unset);

/**
 * @brief Selects a pre-existing base kernel for edge detection.
 * @param kt - The type of edge detection kernel
 * @return An edge detection kernel
 */
Mat selectBaseKernel(kernelType = kernelType::unset);

/**
 * @brief Applies a "double" edge detection filter to an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param gap - The gap between each base kernels
 * @param kgt - Experimental edge detection kernel type
 * @param kt - Type of edge detection effect (The base kernel)
 * @param bt - Type of blurring effect [Optional]
 * @param blurKernelSize - Kernel size of blur filter (Only necessary with GassianCustom blur type)
 */
void experimentalEdgeDetection(Mat, Mat&, int = 3, kernelGenerationType = kernelGenerationType::unset, kernelType = kernelType::unset, blurType = blurType::unset, int = 9);

/**
 * @brief Displays a series of the input image applied with a "double" edge detection filter with n number of different gaps.
 * @param input - Input image matrix
 * @param limit - Upper gap size limit
 * @param stepSize - Step size in gap incrementation
 * @param kgt - Experimental edge detection kernel type
 * @param kt - Type of edge detection effect (The base kernel)
 * @param bt - Type of blurring effect [Optional]
 * @param blurKernelSize - Kernel size of blur filter (Only necessary with GassianCustom blur type)
 * @param tileCols - Number of columns in the rendered tile layout
 * @param label - Label on the image display window [Optional]
 * @param path - Path for image saving [Optional] (Is only saved if a path is given)
 */
void edgeDetectionBruteforce(Mat, int = 10, int = 1, kernelGenerationType = kernelGenerationType::unset, kernelType = kernelType::unset, blurType = blurType::unset, int = 9, int = 3, std::string = "Edge detection bruteforce", std::string path = "");

/**
 * @brief Performs edge detection on an image with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param windowName - Label on the image display window [Optional]
 */
void edgeDetectionWithTrackbar(Mat, Mat&, std::string = "Edge detection with trackbars");

/**
 * @brief Applies a template matching filter to an image.
 * @param input - Input image matrix
 * @param templateMat - Image matrx used as template
 * @param output - Output image matrix
 * @param method - Method of template matching
 * @param displayRect - Should the displayed image feature a rectangle around the hotspot
 */
void templateMatching(Mat, Mat, Mat&, int = 0, bool = true);

/**
 * @brief Performs thresholding on an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param tt - Type of threshold effect
 * @param thresholdValue - Lower threshold value
 * @param maxThresholdValue - Upper threshold value [Optional]
 * @param saltAndPepperValue - Size of median blur kernel [Optional]
 */
void thresholding(Mat, Mat&, thresholdType = thresholdType::unset, int = 0, int = 255, int = 0);

/**
 * @brief Performs thresholding on an image with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param windowName - Label on the image display window [Optional]
 */
void thresholdingWithTrackbar(Mat, Mat&, std::string = "Thresholding with trackbars");

/**
 * @brief Renders a histogram based on the input image.
 * @param input - Input image matrix
 * @param label - Label on the image display window [Optional]
 */
void renderHistogram(Mat, std::string = "Unnamed histogram");

/**
 * @brief Applies a contract filter to an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param ct - Type of contract enhancement
 */
void contrastEnhancement(Mat, Mat&, contrastType = contrastType::unset);

/**
 * @brief Generates a frame structuring element, used for morphology operations.
 * @param cols - Number of columns in the structuring element
 * @param rows - Number of rows in the structuring element
 * @param thickness - Border size of the structuring element
 * @return a hollow rectangle structuring element surrounded by a border
 */
Mat frameStructuringElement(int, int, int);

/**
 * @brief Performs a morphology operation on an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param mt - Type of morphology operation
 * @param ms - Type of morphology shape
 * @param width - Width of selected morphology shape
 * @param height - Height of selected morphology shape
 * @param borderSize - Border size of frame shape (If rectangle frame shape is selected)
 */
void morphology(Mat, Mat&, morphologyType = morphologyType::unset, morphologyShape = morphologyShape::unset, int = 10, int = 10, int = 3);

/**
 * @brief Performs morphology on an image with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param windowName windowName - Label on the image display window [Optional]
 */
void morphologyWithTrackbar(Mat, Mat&, std::string = "Morphology with trackbars");

/**
 * @brief Performs Canny edge detection on an image with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param windowName windowName - Label on the image display window [Optional]
 */
void cannyEdgesWithTrackbar(Mat, Mat&, std::string = "Canny edge detection with trackbars");

/**
 * @brief Finds line segments in a binary image using the probabilistic Hough transform.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param isProbabilistic - Select between standard Hough or probabilistic Hough transformation
 * @param threshold - The minimum number of intersections to "detect" a line
 * @param minLineLength - Minimum length of a Hough line segment
 * @param maxLineLength - Maximum length of a Hough line segment
 * @param density - The resolution of the parameter rho in pixels
 * @param theta - The resolution of the parameter theta in radians
 * @param color - Color of the bounding boxes (Red by default) [Optional]
 */
void houghTransform(Mat, Mat&, bool = true, int = 80, double = 10, double = 30, double = 1.0, double = CV_PI / 180, Scalar = CV_RGB(0, 255, 0));

/**
 * @brief Finds line segments in a binary image using the probabilistic Hough transform with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param windowName windowName - Label on the image display window [Optional]
 */
void houghTransformWithTrackbar(Mat, Mat&, std::string = "Hough transform");

/**
 * @brief Draws rotated rectangles on top of an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param rects - Vector of rotated rectangles
 * @param color - Color of the bounding boxes (Red by default) [Optional]
 */
void drawRectBoundingBoxes(Mat, Mat&, std::vector<RotatedRect>, Scalar = CV_RGB(255, 0, 0));

/**
 * @brief Finds rectangle contours in an image.
 * @param input - Input image matrix
 * @param output - Output image matrix
 * @param boxes - Output rotated rectangles from the contour bounding boxes
 * @param minAreaSize - Minimum acceptable rectangle area
 * @param padding - Extra padding added to found rectangle
 * @param mode - "mode" parameter for the OpenCV function findContours()
 * @param method - "method" parameter for the OpenCV function findContours()
 * @param maxAreaSize - Maximum acceptable rectangle area [Optional]
 */
void findRectContours(Mat, Mat&, std::vector<RotatedRect>&, int = 3000, int = 20, int = 2, int = CHAIN_APPROX_NONE, int = -1);

/**
 * @brief Finds rectangle contours in an image with realtime user-controlled sliders.
 * @param input - Input image matrix
 * @param windowName - Label on the image display window [Optional]
 */
void findRectContoursWithTrackbar(Mat, std::string = "Rectangle contours");

/**
 * @brief Exports ROIs of an image, defined by a set of rotated rectangles.
 * @param input - Input image matrix
 * @param boxes - Vector of rotated rectangles
 * @param borderPadding - Applies n pixels zero padding to the image, to medigate rotated rectangles of canvas [Optional]
 * @return A vector of cropped images
 */
std::vector<Mat> exportRectBoundingBoxes(Mat, std::vector<RotatedRect>, int = 100);

/**
 * @brief Renders a visual representation of seperate BLOBs via the grassfire algorithm.
 * @param input - Input image matrix
 * @param output - Output image matrix
 */
void grassFireAlgorithm(Mat, Mat&);

/**
 * @brief The final vision pipeline algorithm.
 * @param input - Input image matrix
 * @return
 */
Mat singleVisionPipeline(Mat);

/**
 * @brief Takes an image a puts it through a pre-defined vision pipeline.
 * @param input - Input image matrix
 * @return A vector of BLOB matrices
 */
std::vector<Mat> visionPipeline(Mat);

/**
 * @brief Extracts all BLOBs from the input image and represents them as vectors
 * @param input - Input image matrix
 * @return An unordered hash map of BLOBs
 */
std::unordered_map<int, std::vector<objectPixel>> representBlobsAsVectors(Mat);

/**
 * @brief Crops out a rectangular section of a matrix, given label data.
 */
windowBlobs representBlobsWithinLabelsAsVector(Mat, frameData);

/**
 * @brief Exports a vector of images to the filesystem.
 * @param images - A vector of images to be exported
 * @param path - The image file path [Optional]
 * @param baseName - The image file basename (Appended behind index) [Optional]
 * @param extension - The image file extension [Optional]
 */
void exportImages(std::vector<Mat>, std::string = "export", std::string = "exportedImage", std::string = "png");

/**
 * @brief Exports a matrix into a file.
 * @param input - Input image matrix
 * @param fileName - Declares file path, name and document type [Optional]
 */
void exportMatrix(Mat, std::string = "features.txt");

/**
 * @brief Goes through a custom vision pipeline with realtime user-controlled sliders.
 */
void trackbarMode();

/**
 * @brief Goes through the final pre-defined vision pipeline.
 */
void finalPipeline();

/**
 * @brief Goes through the final pre-defined vision pipeline, while showing alternative routes (Meant for generating assets for report/documentation).
 */
void presentationMode();
