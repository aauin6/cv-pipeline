#pragma once

#include "imageProcessing.hpp"

#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>

/**
 * objectClass 0 == windowState::closed
 * objectClass 1 == windowState::open
 */ 
struct distanceToTestData {
    float dist;
    int objectClass;
};

struct videoFeatures {
    std::vector<std::vector<windowFeatures>> videoClosedFeatures;
    std::vector<std::vector<windowFeatures>> videoOpenFeatures;
};

bool compareDistances(distanceToTestData, distanceToTestData);

videoFeatures featureCalculatorForWindowBlobs(std::vector<windowBlobs>);

class knnClassifier {
    private:
        // frames - window - features and state
        videoFeatures trainingData;
        // std::vector<std::vector<windowFeatures>> trainingData;
        // This one is for the tests
        std::vector<std::vector<float>> trainingTestData;
        std::vector<float> maximumValues;
        std::vector<float> minimumValues;
        std::vector<std::vector<float>> dataInitializerTest();
        std::vector<std::vector<float>> dataInitializer(videoFeatures);
        std::vector<float> inputNormalizerTest(std::vector<float>);
        windowFeatures inputNormalizer(windowFeatures);
        void trainingTestDataNormalizer();
        void trainingNormalizer(videoFeatures);
        windowFeatures testDataProcessor(cv::Mat);
   public:
        int classifyWindow(cv::Mat, int);
        int classifyTest(std::vector<float>, int);
    knnClassifier();
    knnClassifier(videoFeatures);
};
    