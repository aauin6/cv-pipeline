#include "knnClassifier.hpp"
#include "imageProcessing.hpp"

int main() {
	const int selectedMode = 1;

	if (selectedMode == 1) presentationMode();
	else if (selectedMode == 2) trackbarMode();
	else if (selectedMode == 3) finalPipeline();

	return 0;
}