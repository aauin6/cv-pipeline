#pragma once

#include "imageProcessing.hpp"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace cv;

/**
 * Here test means "this is data to be classified
 * unset means "has not been set (possibly an error)"
 * Open means "this window is classified as open (if test data) or ground truth open (if training data)
 * Closed means "this window is classified as closed (if test data) or ground truth closed (if training data)
 */
enum class windowState {
	unset,
	test,
	open,
	closed
};

struct windowBlobs;

struct window {
	float upperLeftCornerX;
	float upperLeftCornerY;
	float lowerRightCornerX;
	float lowerRightCornerY;
};

struct frameData {
	std::vector<window> openWindows;
	std::vector<window> closedWindows;
};

struct windowFeatures {
	windowState state = windowState::unset;
	std::vector<float> features;
};

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream&);
std::vector<window> loadSingleCsvFile(std::string);
std::vector<frameData> readAllFiles(int, std::string);
std::vector<windowBlobs> correlateFrames(std::string, std::vector<frameData>);
