#include "csvReader.hpp" 
#include "imageProcessing.hpp"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str) {
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str, line);

    std::stringstream          lineStream(line);
    std::string                cell;
    while (std::getline(lineStream, cell, ',')) {
        result.push_back(cell);
    }

    return result;
}

std::vector<window> loadSingleCsvFile(std::string fn) {
    std::vector<window> windows; std::ifstream file(fn); int counter = 0;
    while (file.good()) {
        counter++;
        window currentWindow = window();
        // The following line reads and discards a line
        getNextLineAndSplitIntoTokens(file);
        if (file.eof()) {
            break;
        }
        std::vector<std::string> data = getNextLineAndSplitIntoTokens(file);
        // The following line reads and discards a line
        getNextLineAndSplitIntoTokens(file);
        data.pop_back();
        // Note: The csv file structure is upper left corner(x, y) followed by delta(x, y). 
        // Hence, the addition of the second value to the zeroth value and the addition of the third value to the first value.
        currentWindow.upperLeftCornerX  = std::stof(data.at(0)); //0
        currentWindow.upperLeftCornerY  = std::stof(data.at(1)); //1
        currentWindow.lowerRightCornerX = std::stof(data.at(2)) + currentWindow.upperLeftCornerX; //2
        currentWindow.lowerRightCornerY = std::stof(data.at(3)) + currentWindow.upperLeftCornerY; //3
        /* This is debug info level: EXTREME!!!
        for (auto& elm : data) {
            std::cout << elm << " ";
        }
        std::cout << "\n";
        std::cout << "The eof value is: " << file.eof() << std::endl;
        */
        windows.push_back(currentWindow);
    }
    return windows;
}

std::vector<frameData> readAllFiles(int numberOfFiles, std::string basePath) {
    std::vector<frameData> totalData;
    std::string extension = ".csv";

    
    for (int index = 1; index < numberOfFiles + 1; index++) {
        std::string closedFile = basePath + "/closed/closed"; 
        std::string openFile = basePath + "/open/open"; 
        closedFile += std::to_string(index); closedFile += extension;
        openFile += std::to_string(index); openFile += extension;

        // Debug info
        // std::cout << openFile << "\n" << closedFile << std::endl;

        frameData frame;
        std::vector<window> openWindows = loadSingleCsvFile(openFile);
        std::vector<window> closedWindows = loadSingleCsvFile(closedFile);
        frame.openWindows = openWindows;
        frame.closedWindows = closedWindows;
        totalData.push_back(frame);
    }
    return totalData;
}

std::vector<windowBlobs> correlateFrames(std::string fn, std::vector<frameData> labels) {
    VideoCapture video = VideoCapture(fn);

    if(!video.isOpened()) {
        std::cerr << "The video file could not be opened, please check the path";
        exit(1);
    }

    Mat frame;
    // exportMatrix(frame, "/home/mikkel/Documents/uni/p6/cv-pipeline/features.txt");
    // Binarize the frame using the methods that are used in the pipeline.
    
    //std::cout << frame.type() << std::endl;
    std::vector<windowBlobs> videoBlobs;
    //std::cout << "The number of labels is: " << labels.size() << std::endl;
    int index = 0;
    for (auto& currentLabel : labels) {
        //std::cout << "Labels processed: " << index << " of " << labels.size() << std::endl;
        video >> frame;
       // if (frame.channels() != 1)
            cvtColor(frame, frame, COLOR_RGB2GRAY);
        Mat processedFrame = singleVisionPipeline(frame);
        //imshow("re", processedFrame);
        //waitKey(0);

        //threshold(frame, frame, 1, 255, THRESH_BINARY);

        /*
        for (int i=0; i<frame.cols; i++) {
            for (int j=0; j<frame.rows; j++) {
                std::cout << (int)frame.at<uchar>(Point(i, j)) << " ";
            }
            std::cout << "\n";
        }
        */

        //std::cout << "frame type: " << frame.type() << ", frame val: " << (int)processedFrame.at<uchar>(72, 28) << std::endl;
        // exportMatrix(processedFrame);
        windowBlobs blobs = representBlobsWithinLabelsAsVector(processedFrame, currentLabel);

        videoBlobs.push_back(blobs);
    }

    return videoBlobs;
}
