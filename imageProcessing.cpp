#include "imageProcessing.hpp"

using namespace cv;

void imageTiling(std::vector<Mat> images, Mat& output, int cols, std::string label) {
    int numOfImages, rowOffset, rows, singleHeight, singleWidth, totalHeight, totalWidth, x = 0, y = 0, i = 0;
    Mat result, currentImage;
    Rect roi;

    numOfImages = images.size();
    rowOffset = numOfImages % cols;

    // Calculate number of rows
    if (rowOffset == 0)
        rows = numOfImages / cols;
    else
        rows = (numOfImages + (cols - rowOffset)) / cols;

    // Calculate total height and width
    singleHeight = images.at(0).rows;
    singleWidth = images.at(0).cols;

    totalHeight = singleHeight * rows;
    totalWidth = singleWidth * cols;
    
    result = Mat(totalHeight, totalWidth, images.at(0).type());

    for (; y < totalHeight; x += singleWidth) {

            if (x >= totalWidth) {
                x = -singleWidth;
                y += singleHeight;
                continue;
            }

            if (i < numOfImages) {
                roi = Rect(x, y, singleWidth, singleHeight);
                currentImage = images.at(i);
                currentImage.copyTo(result(roi));
            }

            i++;
    }
    output = result;

    namedWindow(label, WINDOW_NORMAL);
    imshow(label, result);
}

void blurEffect(Mat input, Mat& output, blurType bt, int kernelSize, bool required) {
    switch (bt) {
        case blurType::mean:
            blur(input, output, Size(kernelSize, kernelSize));
            break;
        case blurType::median:
            medianBlur(input, output, kernelSize);
            break;
        case blurType::gaussian:
            GaussianBlur(input, output, Size(kernelSize, kernelSize), 0, 0);
            break;
        case blurType::bilateral:
            bilateralFilter(input, output, 9, kernelSize, kernelSize, BORDER_DEFAULT);
            break;
        default:
            if (required == false) output = input;
            else {
                std::cerr << "\nThis function must have a blur effect type!" << std::endl;
                exit(-1);
            }
            break;
    }
}

void edgeDetection(Mat input, Mat& output, kernelType kt, blurType bt, int kernelSize) {
    Mat inputBlurred, grad_x, grad_y, abs_grad_x, abs_grad_y;
    int ddepth = CV_16S;

    // Optional pre-bluring effect
    blurEffect(input, inputBlurred, bt, kernelSize, false);

    switch (kt) {
		case kernelType::sobel:
			Sobel(inputBlurred, grad_x, ddepth, 1, 0);
			convertScaleAbs(grad_x, abs_grad_x);
			Sobel(inputBlurred, grad_y, ddepth, 0, 1);
			convertScaleAbs(grad_y, abs_grad_y);

			addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, output);
			break;
		case kernelType::scharr:
			Scharr(inputBlurred, grad_x, ddepth, 1, 0);
			convertScaleAbs(grad_x, abs_grad_x);
			Scharr(inputBlurred, grad_y, ddepth, 0, 1);
			convertScaleAbs(grad_y, abs_grad_y);

			addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, output);
			break;
        case kernelType::laplacian: {
            // https://aishack.in/tutorials/sobel-laplacian-edge-detectors/
            Mat laplacianKernel = (Mat_<float>(3, 3) << 0, -1, 0, -1, 4, -1, 0, -1, 0);
            filter2D(inputBlurred, output, -1, laplacianKernel);
            convertScaleAbs(output, output);
            break;
        }
        case kernelType::laplacianWithDiagonals: {
            // https://aishack.in/tutorials/sobel-laplacian-edge-detectors/
            Mat laplacianWithDiagonalsKernel = (Mat_<float>(3, 3) << -1, -1, -1, -1, 8, -1, -1, -1, -1);
            filter2D(inputBlurred, output, -1, laplacianWithDiagonalsKernel);
            convertScaleAbs(output, output);
            break;
        }     
        default:
            std::cerr << "\nPlease choose a kernel type" << std::endl;
            exit(-1);
    }
}

Mat kernelGenerator(int gap, Mat baseKernel, orientationType dir, kernelGenerationType kgt) {
    Mat flippedBaseKernel;
    int newKernelSize;

    flip(baseKernel, flippedBaseKernel, -1);

    if (baseKernel.rows != baseKernel.cols) {
        std::cerr << "Input kernel must be square.";
        exit(1);
    }

    int baseKernelSize = baseKernel.rows;

    if (kgt == kernelGenerationType::doubleEdge)
        newKernelSize = (baseKernelSize * 2) + gap;
    else
        newKernelSize = gap + 2;

    Mat kernel = Mat::zeros(baseKernelSize, newKernelSize, CV_32F);
    Mat transposedKernel = Mat::zeros(newKernelSize, baseKernelSize, CV_32F);

    if (kgt == kernelGenerationType::doubleEdge) {
        for (int row = 0; row < baseKernelSize; row++) {
            for (int col = 0; col < newKernelSize; col++) {
                if (col < baseKernelSize)
                    kernel.at<float>(row, col) = baseKernel.at<float>(row, col);
                else if (col >= (baseKernelSize + gap))
                    kernel.at<float>(row, col) = flippedBaseKernel.at<float>(row, col - (baseKernelSize + gap));
            }
        }
    } else if (kgt == kernelGenerationType::thickEdge) {
        for (int row = 0; row < baseKernelSize; row++) {
            for (int col = 0; col < newKernelSize; col++) {
                if (col == 0)
                    kernel.at<float>(row, col) = baseKernel.at<float>(row, 0);
                else if (col == newKernelSize - 1)
                    kernel.at<float>(row, col) = baseKernel.at<float>(row, baseKernelSize - 1);
            }
        }
    }
    transpose(kernel, transposedKernel);

    if (dir == orientationType::horizontal)
        return transposedKernel;
    return kernel;
}

Mat selectBaseKernel(kernelType kt) {
    Mat selectedKernel;

    switch (kt) {
        case kernelType::sobel: {
            Mat_<float> sobel(3, 3);
            sobel << 1, 0, -1, 2, 0, -2, 1, 0, -1;

            selectedKernel = sobel;
            break;
        }
        case kernelType::scharr: {
            Mat_<float> scharr(3, 3);
            scharr << 3, 0, -3, 10, 0, -10, 3, 0, -3;

            selectedKernel = scharr;
            break;
        }
        case kernelType::strongScharr: {
            Mat_<float> scharr(3, 3);
            scharr << 47, 0, -47, 162, 0, -162, 47, 0, -47;

            selectedKernel = scharr;
            break;
        }
        case kernelType::laplacian: {
            Mat_<float> laplacian(3, 3);
            laplacian << 0, -1, 0, -1, 4, -1, 0, -1, 0;

            selectedKernel = laplacian;
            break;
        }
        case kernelType::laplacianWithDiagonals: {
            selectedKernel = (Mat_<float>(3, 3) << -1, -1, -1, -1, 8, -1, -1, -1, -1);
            break;
        }
        default:
            std::cerr << "\nPlease select a base kernel" << std::endl;
            exit(-1);
    }

    return selectedKernel;
}

void experimentalEdgeDetection(Mat input, Mat& output, int gap, kernelGenerationType kgt, kernelType kt, blurType bt, int blurKernelSize) {
    Mat verticalKernel, horizontalKernel, verticalApplied, horizontalApplied, baseKernel, inputBlurred;
    int depth = input.depth();
    float weight = 1.0;

    // Optional pre-bluring effect
    blurEffect(input, inputBlurred, bt, blurKernelSize, false);

    // Generate kernels
    baseKernel = selectBaseKernel(kt);
    verticalKernel = kernelGenerator(gap, baseKernel, orientationType::vertical, kgt);
    horizontalKernel = kernelGenerator(gap, baseKernel, orientationType::horizontal, kgt);

    // Apply Vertical kernel
    filter2D(inputBlurred, verticalApplied, depth, verticalKernel);
    convertScaleAbs(verticalApplied, verticalApplied);

    // Apply Horizontal kernel
    filter2D(inputBlurred, horizontalApplied, depth, horizontalKernel);
    convertScaleAbs(horizontalApplied, horizontalApplied);

    // Merge Horizontal & Vertical kernels
    addWeighted(horizontalApplied, weight, verticalApplied, weight, 0, output);
}

void edgeDetectionBruteforce(Mat input, int limit, int stepSize, kernelGenerationType kgt, kernelType kt, blurType bt, int blurKernelSize, int tileCols, std::string label, std::string path) {
    std::vector<Mat> results;
    Mat output;

    for (size_t i = 0; i < limit; i += stepSize) {
        Mat temp;
        experimentalEdgeDetection(input, temp, i, kgt, kt, bt, blurKernelSize);
        results.push_back(temp);
    }

    imageTiling(results, output, tileCols, label);

    if (!path.empty())
        imwrite(path, output);
}

void edgeDetectionWithTrackbar(Mat input, Mat& output, std::string windowName) {
    Mat tempOutput;
    const int maxBlurringTypeID = 3, maxBlurringValue = 150, maxExperimentalGap = 25, maxEDTypeID = 3, maxExperimentalTypeID = 2;
    int blurringTypeID = 0, blurringSlider = 6, experimentalGapSlider = 3, experimentalTypeID = 0, selectedTypeID = 0;
    blurType blurringType = blurType::unset;
    kernelType selectedType = kernelType::unset;
    kernelGenerationType selectedExperimentalType = kernelGenerationType::unset;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Blurring kernel:\n 0: gaussian \n 1: mean \n 2: median \n 3: bilateral", windowName, &blurringTypeID, maxBlurringTypeID);
    createTrackbar("Blurring size", windowName, &blurringSlider, maxBlurringValue);
    createTrackbar("ED kernel:\n 0: sobel \n 1: scharr \n 2: laplacian \n 3: laplacianWithDiagonals", windowName, &selectedTypeID, maxEDTypeID);

    createTrackbar("Experimental kernel type:\n 0: None \n 1: Double ED \n 2: Thick ED", windowName, &experimentalTypeID, maxExperimentalTypeID);
    createTrackbar("Experimental gap", windowName, &experimentalGapSlider, maxExperimentalGap);

    while (true) {
        if (blurringSlider < 1) blurringSlider = 1;

        if (selectedTypeID == 0) selectedType = kernelType::sobel;
        else if (selectedTypeID == 1) selectedType = kernelType::scharr;
        else if (selectedTypeID == 2) selectedType = kernelType::laplacian;
        else if (selectedTypeID == 3) selectedType = kernelType::laplacianWithDiagonals;

        if (blurringTypeID == 0) blurringType = blurType::gaussian;
        else if (blurringTypeID == 1) blurringType = blurType::mean;
        else if (blurringTypeID == 2) blurringType = blurType::median;
        else if (blurringTypeID == 3) blurringType = blurType::bilateral;

        if (blurringTypeID == 0 || blurringTypeID == 2)
            blurringSlider = blurringSlider % 2 == 0 ? blurringSlider + 1 : blurringSlider;

        if (experimentalTypeID != 0) {
            if (experimentalTypeID == 1) selectedExperimentalType = kernelGenerationType::doubleEdge;
            else if (experimentalTypeID == 2) selectedExperimentalType = kernelGenerationType::thickEdge;

            experimentalEdgeDetection(input, tempOutput, experimentalGapSlider, selectedExperimentalType, selectedType, blurringType, blurringSlider);
        }
        else edgeDetection(input, tempOutput, selectedType, blurringType, blurringSlider);

        imshow(windowName, tempOutput);

        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }
    output = tempOutput;
}

void templateMatching(Mat input, Mat templateMat, Mat& output, int method, bool displayRect) {
    int methods[] = { TM_SQDIFF , TM_SQDIFF_NORMED, TM_CCORR, TM_CCORR_NORMED, TM_CCOEFF, TM_CCOEFF_NORMED };
    
    matchTemplate(input, templateMat, output, methods[method]);
    normalize(output, output, 0, 1, NORM_MINMAX, -1, Mat());

    if (displayRect == true) {
        double minVal, maxVal;
        Point minLoc, maxLoc, matchLoc;

        minMaxLoc(output, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

        if (method == TM_SQDIFF || method == TM_CCORR_NORMED) matchLoc = minLoc;
        else matchLoc = maxLoc;

        rectangle(output, matchLoc, Point(matchLoc.x + templateMat.cols, matchLoc.y + templateMat.rows), Scalar::all(0), 2, 8, 0);
        //rectangle(result, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), Scalar::all(0), 2, 8, 0);
    }
}

void thresholding(Mat input, Mat& output, thresholdType tt, int thresholdValue, int maxThresholdValue, int saltAndPepperValue) {
    Mat tempInput = input.clone();

    if (saltAndPepperValue > 0)
        blurEffect(tempInput, tempInput, blurType::median, saltAndPepperValue);

    switch(tt) {
        case thresholdType::adaptiveMean:
            adaptiveThreshold(tempInput, output, maxThresholdValue, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 11, 10);
            break;
        case thresholdType::adaptiveGaussian:
            adaptiveThreshold(tempInput, output, maxThresholdValue, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 11, 10);
            break;
        case thresholdType::otsu: // Otsu's thresholding
            threshold(tempInput, output, thresholdValue, maxThresholdValue, THRESH_OTSU);
            break;
        case thresholdType::binary: // Global (binary) thresholding
            threshold(tempInput, output, thresholdValue, maxThresholdValue, THRESH_BINARY);
            break;
        default:
            std::cout << "\nPlease choose a threshold type" << std::endl;
            exit(-1);
	}
}

void thresholdingWithTrackbar(Mat input, Mat& output, std::string windowName) {
    Mat tempOutput;
    const int maxThresholdValue = 255, maxThresholdTypeID = 3, maxBlurringValue = 50;
    int thresholdSlider = 0, selectedTypeID = 0, blurringSlider = 0;
    thresholdType selectedType = thresholdType::unset;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Threshold value", windowName, &thresholdSlider, maxThresholdValue);
    createTrackbar("Element:\n 0: Binary \n 1: Adaptive Mean \n 2: Adaptive Gaussian \n 3: Otsu", windowName, &selectedTypeID, maxThresholdTypeID);
    createTrackbar("Salt and Pepper value", windowName, &blurringSlider, maxBlurringValue);

    while (true) {
        blurringSlider = blurringSlider % 2 == 0 ? blurringSlider + 1 : blurringSlider;

        if (selectedTypeID == 0) selectedType = thresholdType::binary;
        else if (selectedTypeID == 1) selectedType = thresholdType::adaptiveMean;
        else if (selectedTypeID == 2) selectedType = thresholdType::adaptiveGaussian;
        else if (selectedTypeID == 3) selectedType = thresholdType::otsu;
        
        thresholding(input, tempOutput, selectedType, thresholdSlider, maxThresholdValue, blurringSlider);
        imshow(windowName, tempOutput);

        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }

    output = tempOutput;
}

void renderHistogram(Mat input, std::string label) {
    Mat output;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    int channels[] = {0, 1};
    bool uniform = true; bool accumulate = false;
    int numberOfBins = 256;
    calcHist(&input, 1, channels, Mat(), output, 1, &numberOfBins, &histRange, uniform, accumulate);

    // Draw the histogram
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound((double)hist_w / numberOfBins);

    Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

    /// Normalize the result to [ 0, histImage.rows ]
    normalize(output, output, 0, histImage.rows, NORM_MINMAX, -1, Mat());
    for (int i = 1; i < numberOfBins; i++)
    {
        line(histImage, Point(bin_w * (i - 1), hist_h - cvRound(output.at<float>(i - 1))),
            Point(bin_w * (i), hist_h - cvRound(output.at<float>(i))),
            Scalar(255, 255, 255), 2, 8, 0);
    }
    //namedWindow(label, CV_WINDOW_AUTOSIZE);
    imshow(label, histImage);
}

void contrastEnhancement(Mat input, Mat& output, contrastType ct) {
    double min, max;
    minMaxLoc(input, &min, &max); // Calculate minimum and maximum intensity of input matrix

    switch(ct) {
        case contrastType::histogramStretching: // Using histogram stretching
            output = ((input - min) / (max - min)) * 255;
            break;
        default:
            std::cout << "\nPlease choose a contrast type" << std::endl;
            exit(-1);
	}
}

Mat frameStructuringElement(int cols, int rows, int thickness) {
    /// Size takes (cols, rows)... Even though we usually say RC for rows, cols...
    Mat mat = Mat::ones(Size(cols, rows), 0);

    // int limit = mat.rows < mat.cols ? mat.rows : mat.cols;
    for (int j = 0; j < thickness; j++) {

        for (int i = 0; i < mat.rows; i++) {
            mat.at<uchar>(i, j) = 0;
            mat.at<uchar>(i, mat.cols - (1 + j)) = 0;
        }
        for (int i = 0; i < mat.cols; i++) {
            mat.at<uchar>(j, i) = 0;
            mat.at<uchar>(mat.rows - (1 + j), i) = 0;
        }
    }
    return mat;
}

void morphology(Mat input, Mat& output, morphologyType mt, morphologyShape ms, int width, int height, int borderSize) {
    Mat structureElement, transposedStructureElement, morphX, morphY;

    switch (ms) {
        case morphologyShape::cross:
            structureElement = getStructuringElement(MORPH_CROSS, Size(width, height));
            transposedStructureElement = getStructuringElement(MORPH_CROSS, Size(height, width));
            break;
        case morphologyShape::rect:
            structureElement = getStructuringElement(MORPH_RECT, Size(width, height));
            transposedStructureElement = getStructuringElement(MORPH_RECT, Size(height, width));
            break;
        case morphologyShape::rectFrame:
            structureElement = frameStructuringElement(width, height, borderSize);
            transposedStructureElement = frameStructuringElement(height, width, borderSize);
            break;
        default:
            std::cerr << "\nPlease choose a morphology shape!" << std::endl;
            exit(-1);
    }

    switch (mt) {
        case morphologyType::open:
            morphologyEx(input, output, MORPH_OPEN, structureElement);
            break;
        case morphologyType::close:
            morphologyEx(input, output, MORPH_CLOSE, structureElement);
            break;
        case morphologyType::erode:
            erode(input, output, structureElement);
            break;
        case morphologyType::dilate:
            dilate(input, output, structureElement);
            break;
        case morphologyType::combine: {
            morphologyEx(input, morphX, MORPH_OPEN, structureElement);
            morphologyEx(input, morphY, MORPH_OPEN, transposedStructureElement);
            addWeighted(morphX, 1, morphY, 1, 0, output);
            break;
        }
        case morphologyType::extend: {
            //dilate(input, morphX, structureElement);
            //dilate(input, morphY, transposedStructureElement);
            morphologyEx(input, morphX, MORPH_CLOSE, structureElement);
            morphologyEx(input, morphY, MORPH_CLOSE, transposedStructureElement);
            addWeighted(morphX, 1, morphY, 1, 0, output);
            break;
        }
        case morphologyType::isolateLines: {
            erode(input, morphX, structureElement);
            dilate(morphX, morphX, structureElement);
            erode(input, morphY, transposedStructureElement);
            dilate(morphY, morphY, transposedStructureElement);
            addWeighted(morphX, 1, morphY, 1, 0, output);
            break;
        }
        default:
            std::cerr << "\nPlease choose a morphology operation!" << std::endl;
            exit(-1);
	}
}

void morphologyWithTrackbar(Mat input, Mat& output, std::string windowName) {
    Mat tempOutput;
    const int maxWidth = 100, maxHeight = 100, maxShapeID = 2, maxBorderSize = 10, maxTypeID = 6;
    int widthSlider = 5, heightSlider = 5, selectedShapeID = 0, borderSize = 0, selectedTypeID = 2;
    morphologyShape selectedShape = morphologyShape::unset;
    morphologyType selectedType = morphologyType::unset;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Structuring element:\n 0: Rectangle \n 1: Rectangle Frame (Our implementation) \n 2: Cross", windowName, &selectedShapeID, maxShapeID);
    createTrackbar("Morphology type:\n 0: Open \n 1: Close \n 2: Erode \n 3: Dilate \n 4: Open (destroy) \n 5: Dilate (combine) \n 6: isolateLines", windowName, &selectedTypeID, maxTypeID);
    createTrackbar("Width:", windowName, &widthSlider, maxWidth);
    createTrackbar("Height:", windowName, &heightSlider, maxHeight);
    createTrackbar("Border size:\n (only works with nr. 1 structuring element)", windowName, &borderSize, maxBorderSize);

    while (true) {
        if (widthSlider < 1) widthSlider = 1;
        if (heightSlider < 1) heightSlider = 1;

        if (selectedShapeID == 0) selectedShape = morphologyShape::rect;
        else if (selectedShapeID == 1) selectedShape = morphologyShape::rectFrame;
        else if (selectedShapeID == 2) selectedShape = morphologyShape::cross;

        if (selectedTypeID == 0) selectedType = morphologyType::open;
        else if (selectedTypeID == 1) selectedType = morphologyType::close;
        else if (selectedTypeID == 2) selectedType = morphologyType::erode;
        else if (selectedTypeID == 3) selectedType = morphologyType::dilate;
        else if (selectedTypeID == 4) selectedType = morphologyType::combine;
        else if (selectedTypeID == 5) selectedType = morphologyType::extend;
        else if (selectedTypeID == 6) selectedType = morphologyType::isolateLines;

        morphology(input, tempOutput, selectedType, selectedShape, widthSlider, heightSlider, borderSize);

        imshow(windowName, tempOutput);

        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }

    output = tempOutput;
}

void cannyEdgesWithTrackbar(Mat input, Mat& output, std::string windowName) {
    Mat tempOutput;
    const int maxThreshold = 255, apertureSizeMax = 100;
    int lowerThresholdSlider = 50, upperThresholdSlider = 200, apertureSize = 3;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Lower threshold:", windowName, &lowerThresholdSlider, maxThreshold);
    //createTrackbar("Upper threshold:", windowName, &upperThresholdSlider, maxThreshold);
    createTrackbar("Aperture size:", windowName, &apertureSize, apertureSizeMax);

    while (true) {
        if (apertureSize < 1) apertureSize = 1;
        apertureSize = apertureSize % 2 == 0 ? apertureSize + 1 : apertureSize;

        Canny(input, tempOutput, (double)lowerThresholdSlider, (double)lowerThresholdSlider * 3, apertureSize);

        imshow(windowName, tempOutput);

        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }

    output = tempOutput;
}

void houghTransform(Mat input, Mat& output, bool isProbabilistic, int threshold, double minLineLength, double maxLineLength, double density, double theta, Scalar color) {
    Mat tempOutput, in;
    std::vector<Vec4i> linesP;
    std::vector<Vec2f> lines;

    // Find the edges in the image using canny detector
    //Canny(input, in, 50, 200, 3);

    cvtColor(input, tempOutput, COLOR_GRAY2BGR);
    
    if (isProbabilistic) { 
        // Apply probabilistic Hough Transform
        HoughLinesP(input, linesP, density, theta, threshold, minLineLength, maxLineLength);

       // cvtColor(input, tempOutput, COLOR_GRAY2BGR);
        // Draw lines on the image
        for (size_t i = 0; i < linesP.size(); i++) {
            Vec4i l = linesP[i];
            line(tempOutput, Point(l[0], l[1]), Point(l[2], l[3]), color, 3, LINE_AA);
        }
    }
    else {
        // Apply normal Hough Transform
        HoughLines(input, lines, density, theta, threshold, 0, 0);
       // cvtColor(input, tempOutput, COLOR_GRAY2BGR);
        // Draw lines on the image
        for (size_t i = 0; i < lines.size(); i++) {
            float rho = lines[i][0], theta2 = lines[i][1];
            Point pt1, pt2;
            double a = cos(theta2), b = sin(theta2);
            double x0 = a * rho, y0 = b * rho;
            pt1.x = cvRound(x0 + 1000 * (-b));
            pt1.y = cvRound(y0 + 1000 * (a));
            pt2.x = cvRound(x0 - 1000 * (-b));
            pt2.y = cvRound(y0 - 1000 * (a));
            line(tempOutput, pt1, pt2, color, 3, LINE_AA);
        }
    }
    
    output = tempOutput;
}

void houghTransformWithTrackbar(Mat input, Mat& output, std::string windowName) {
    Mat tempOutput;
    const int maxTreshold = 255, maxLineWidth = 200, maxDensity = 10, maxHoughMode = 1;
    int thresholdSlider = 5, minLineSlider = 5.0, maxLineSlider = 100.0, densitySlider = 1, houghMode = 1;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Hough mode:\n 0: Normal \n 1: Probabilistic", windowName, &houghMode, maxHoughMode);
    createTrackbar("Threshold", windowName, &thresholdSlider, maxTreshold);
    createTrackbar("Min line size", windowName, &minLineSlider, maxLineWidth);
    createTrackbar("Max line size", windowName, &maxLineSlider, maxLineWidth);
    createTrackbar("Density", windowName, &densitySlider, maxDensity);

    while (true) {
        if (densitySlider < 1) densitySlider = 1;

        houghTransform(input, tempOutput, houghMode, thresholdSlider, (double)minLineSlider, (double)maxLineSlider, (double)densitySlider);

        imshow(windowName, tempOutput);

        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }

    cvtColor(tempOutput, tempOutput, COLOR_BGR2GRAY);
    threshold(tempOutput, tempOutput, 1, 255, THRESH_BINARY);

    output = tempOutput;
}

void drawRectBoundingBoxes(Mat input, Mat& output, std::vector<RotatedRect> rects, Scalar color) {
    Mat coloredInput;

    cvtColor(input, coloredInput, COLOR_GRAY2BGR);

    for (RotatedRect rr : rects) {
        Point2f points[4];
        rr.points(points);

        for (int i = 0; i < 4; i++) {
            int ii = (i + 1) % 4;
            line(coloredInput, points[i], points[ii], color, 2);
        }
    }
    output = coloredInput;
}

void findRectContours(Mat input, Mat& output, std::vector<RotatedRect>& boxes, int minAreaSize, int padding, int mode, int method, int maxAreaSize) {
    std::vector<std::vector<Point>> contours;
    std::vector<Vec4i> hierarchy;
    std::vector<RotatedRect> rects;

    Mat contourOutput = input.clone();

    int modes[] = { RETR_EXTERNAL, RETR_LIST, RETR_CCOMP, RETR_TREE };
    int methods[] = { CHAIN_APPROX_NONE, CHAIN_APPROX_SIMPLE, CHAIN_APPROX_TC89_L1, CHAIN_APPROX_TC89_KCOS };

    findContours(contourOutput, contours, hierarchy, modes[mode], methods[method]);

    for (int i = 0; i < contours.size(); i++) {

        if (hierarchy[i][2] > 0) continue;

        RotatedRect rr = minAreaRect(contours[i]);
        if (maxAreaSize == -1) {
            if (rr.size.area() < minAreaSize)
                continue;
        } else if (rr.size.area() < minAreaSize || rr.size.area() > maxAreaSize)
            continue;
        else if ((rr.size.height * 2) <= rr.size.width || (rr.size.width * 2) <= rr.size.height)
            continue;
        else if (abs(rr.angle) > 10 && abs(rr.angle) < 80)
            continue;

        if (DEBUG_MODE) std::cout << "Angle: " << rr.angle << "\n";
        if (DEBUG_MODE) std::cout << "Rect found with area: " << rr.size.area() << "\n";

        rr.size.width += padding;
        rr.size.height += padding;
        rects.push_back(rr);
    }

    drawRectBoundingBoxes(contourOutput, output, rects);
    boxes = rects;
}

void findRectContoursWithTrackbar(Mat input, std::string windowName) {
    Mat tempOutput;
    std::vector<RotatedRect> tempRects;
    const int maxLSlider = 20000, maxUSlider = 50000, maxModes = 3, maxMethods = 3, maxPadding = 50;
    int lSlider = 3000, uSlider = 15000, paddingSlider = 10, modeSlider = 2, methodSlider = 0;

    namedWindow(windowName, WINDOW_AUTOSIZE);

    createTrackbar("Lower Threshold", windowName, &lSlider, maxLSlider);
    createTrackbar("Upper Threshold", windowName, &uSlider, maxUSlider);
    createTrackbar("Padding size", windowName, &paddingSlider, maxPadding);
    createTrackbar("Contour mode:\n 0: RETR_EXTERNAL \n 1: RETR_LIST \n 2: RETR_CCOMP \n 3: RETR_TREE", windowName, &modeSlider, maxModes);
    createTrackbar("Contour method:\n 0: CHAIN_APPROX_NONE \n 1: CHAIN_APPROX_SIMPLE \n 2: CHAIN_APPROX_TC89_L1 \n 3: CHAIN_APPROX_TC89_KCOS", windowName, &methodSlider, maxMethods);

    while (true) {
        findRectContours(input, tempOutput, tempRects, lSlider, paddingSlider, modeSlider, methodSlider, uSlider);

        imshow(windowName, tempOutput);
        
        int keyPress = waitKey(50);
        if (keyPress == 27 || keyPress == 13) break;
    }
}

std::vector<Mat> exportRectBoundingBoxes(Mat input, std::vector<RotatedRect> boxes, int borderPadding) {
    std::vector<Mat> temp;
    Mat tempInput;

    copyMakeBorder(input, tempInput, borderPadding, borderPadding, borderPadding, borderPadding, BORDER_CONSTANT, 0);

    for (RotatedRect box : boxes) {
        Rect roi = box.boundingRect();
        Point offset = Point(borderPadding, borderPadding);
        roi += offset;
        Mat boundingRect = tempInput(roi);
        temp.push_back(boundingRect);
    }

    return temp;
}

void grassFireAlgorithm(Mat input, Mat& output) {
    Mat labels = Mat(input.size(), CV_32S), stats, centroids;
    int numsOfLabels = connectedComponentsWithStats(input, labels, stats, centroids, 8, CV_32S);
    std::vector<Vec3b> colors(numsOfLabels);
    colors[0] = Vec3b(0, 0, 0);

    for (int label = 1; label < numsOfLabels; ++label)
        colors[label] = Vec3b(rand() % 256, rand() % 256, rand() % 256);

    output = Mat(input.size(), CV_8UC3);

    for (int row = 0; row < output.rows; ++row) {
        for (int col = 0; col < output.cols; ++col) {
            int label = labels.at<int>(row, col);
            Vec3b& pixel = output.at<Vec3b>(row, col);
            pixel = colors[label];
        }
    }
    exportMatrix(labels, "features.txt");
}

Mat singleVisionPipeline(Mat input) {
    Mat inputContrasted, edgeDetection, binaryThreshold, output;
    contrastEnhancement(input, inputContrasted, contrastType::histogramStretching);
    experimentalEdgeDetection(inputContrasted, edgeDetection, 0, kernelGenerationType::doubleEdge, kernelType::laplacianWithDiagonals, blurType::mean, 3);
    thresholding(edgeDetection, binaryThreshold, thresholdType::binary);
    morphology(binaryThreshold, output, morphologyType::isolateLines, morphologyShape::rect, 35, 1);
    return binaryThreshold;
}

std::vector<Mat> visionPipeline(Mat input) {
    Mat inputContrasted, edgeDetection, binaryThreshold, morphClose, morphOpen, output;
    std::vector<RotatedRect> rects;
    std::vector<Mat> images;
    
    contrastEnhancement(input, inputContrasted, contrastType::histogramStretching);

    experimentalEdgeDetection(inputContrasted, edgeDetection, 0, kernelGenerationType::doubleEdge, kernelType::laplacianWithDiagonals, blurType::mean, 3);

    thresholding(edgeDetection, binaryThreshold, thresholdType::binary);

    morphology(binaryThreshold, morphClose, morphologyType::isolateLines, morphologyShape::rect, 35, 1);

    morphology(morphClose, morphOpen, morphologyType::extend, morphologyShape::rect, 10, 1);

    findRectContours(morphOpen, output, rects, 3000, 10, 2, 1, 10000);

    images = exportRectBoundingBoxes(inputContrasted, rects);

    exportImages(images);

    return exportRectBoundingBoxes(binaryThreshold, rects);
}

windowBlobs representBlobsWithinLabelsAsVector(Mat input, frameData labels) {
    windowBlobs output;
    std::vector<std::vector<objectPixel>> closedWindows;
    std::vector<std::vector<objectPixel>> openWindows;
    std::vector<window> closedWindowLabels = labels.closedWindows;
    std::vector<window> openWindowLabels = labels.openWindows;
    //imshow("tester", input);
    //waitKey(0);
    //std::cout << "height: " << input.rows << ", width: " << input.cols;

	for (auto& window : closedWindowLabels) {
        int xSize, ySize;
        xSize = window.lowerRightCornerX - window.upperLeftCornerX;
        ySize = window.lowerRightCornerY - window.upperLeftCornerY;
        //std::cout << "height: " << xSize << ", width: " << ySize;
        //extractedBlob = Mat::zeros(xSize, ySize, CV_8U);
        std::vector<objectPixel> currentWindow;
		for (int i = window.upperLeftCornerX; i < window.lowerRightCornerX; i++) {
			for (int j = window.upperLeftCornerY; j < window.lowerRightCornerY; j++) {
                //std::cout << "i: " << i << " j: " << j << " val: " << (int)input.at<uchar>(j, i) << "\n";
				objectPixel pixel = { i, j, (int)input.at<uchar>(j, i) };
				currentWindow.push_back(pixel);
			}
		}
	closedWindows.push_back(currentWindow);
	}

	for (auto& window : openWindowLabels) {
        //int xSize, ySize;
        //xSize = window.lowerRightCornerX - window.upperLeftCornerX;
        //ySize = window.lowerRightCornerY - window.upperLeftCornerY;
        //extractedBlob = Mat::zeros(xSize, ySize, CV_8U);
        std::vector<objectPixel> currentWindow;
		for (int i = window.upperLeftCornerX; i < window.lowerRightCornerX; i++) {
			for (int j = window.upperLeftCornerY; j < window.lowerRightCornerY; j++) {
                //std::cout << input.at<uchar>(i, j) << ", " << std::endl;
				objectPixel pixel = { i, j, (int)input.at<uchar>(j, i) };
                    //std::cout << "\nThe col num is: " << pixel.col << "\nThe row num is: " << pixel.row << "\nThe pixel value is: " << pixel.value << std::endl; 
				currentWindow.push_back(pixel);
			}
		}
	openWindows.push_back(currentWindow);
	}


    output.closedWindowBlobs = closedWindows;
    output.openWindowBlobs = openWindows;
    return output;
}

std::unordered_map<int, std::vector<objectPixel>> representBlobsAsVectors(Mat input) {
    std::unordered_map<int, std::vector<objectPixel>> hmap;
    for (int i = 0; i < input.rows; i++) {
        for (int j = 0; j < input.cols; j++) {
            if (input.at<uchar>(i, j) != 0) {
                objectPixel pixel = { i, j, (int)input.at<uchar>(i, j) };
                try {
                    std::vector<objectPixel>& objectVector = hmap.at(pixel.value);
                    objectVector.push_back(pixel);
                }
                catch (std::out_of_range) {
                    std::vector<objectPixel> objectVector;
                    objectVector.push_back(pixel);
                    hmap.insert({ pixel.value, objectVector });
                }
            }
        }
    }
    return hmap;
}

void exportImages(std::vector<Mat> images, std::string path, std::string baseName, std::string extension) {
    int n = 1;

    for (Mat image : images) {
        std::string fileName = path + "/" + std::to_string(n) + "-" + baseName + "." + extension;
        imwrite(fileName, image);
        n++;
    }
}

void exportMatrix(Mat input, std::string fileName) {
    std::ofstream file;
    file.open(fileName);
    file << input << std::endl;
    file.close();
}

void trackbarMode() {
    Mat input, inputContrasted, output, edge, thresh, morphClose, morphOpen, morphCloseAgain;

    input = imread("windowFacade.png", IMREAD_GRAYSCALE);

    contrastEnhancement(input, inputContrasted, contrastType::histogramStretching);

    edgeDetectionWithTrackbar(inputContrasted, edge, "Edge detection");

    contrastEnhancement(edge, edge, contrastType::histogramStretching);

    thresholdingWithTrackbar(edge, thresh, "Thresholding");

    morphologyWithTrackbar(thresh, morphClose, "Morph step #1");

    morphologyWithTrackbar(morphClose, morphOpen, "Morph step #2");

    findRectContoursWithTrackbar(morphOpen);

    imshow("Final result", morphOpen);

    waitKey(0);
}

void finalPipeline() {
    Mat input, inputContrasted, edgeDetection, binaryThreshold, morphClose, morphOpen, output;
    std::vector<RotatedRect> rects;

    input = imread("windowFacade.png", IMREAD_GRAYSCALE);

    contrastEnhancement(input, inputContrasted, contrastType::histogramStretching);

    experimentalEdgeDetection(inputContrasted, edgeDetection, 0, kernelGenerationType::doubleEdge, kernelType::laplacianWithDiagonals, blurType::mean, 3);

    thresholding(edgeDetection, binaryThreshold, thresholdType::binary);

    morphology(binaryThreshold, morphClose, morphologyType::isolateLines, morphologyShape::rect, 35, 1);

    morphology(morphClose, morphOpen, morphologyType::extend, morphologyShape::rect, 10, 1);

    findRectContours(morphOpen, output, rects, 3000, 10, 2, 1, 10000);

    std::vector<Mat> images = exportRectBoundingBoxes(morphOpen, rects);

    exportImages(images);

    waitKey(0);
}

void presentationMode() {
    // Initial variables
    bool showContrastStep = false,
        showBlurStep = false,
        showTemplateStep = false,
        showEdgeStep = true,
        showThresholdStep = false,
        showMorphStep = true,
        showBoundingBoxStep = true,
        saveResults = true;

    Mat thresh, threshII, grassMat;

    // Get input image
    Mat input = imread("windowFacade.png", IMREAD_GRAYSCALE);

    // Contrast enhancement
    Mat inputContrasted;

    contrastEnhancement(input, inputContrasted, contrastType::histogramStretching);

    if (showContrastStep) {
        imshow("Input image", input);
        renderHistogram(input, "Input image histogram (before)");
        imshow("Input image (contrast)", inputContrasted);
        renderHistogram(inputContrasted, "Input image histogram (after)");
    }

    // Blurring
    Mat meanBlurred, bilateralBlurred, medianBlurred, gaussianBlurred;

    blurEffect(inputContrasted, gaussianBlurred, blurType::gaussian, 9);
    blurEffect(inputContrasted, meanBlurred, blurType::mean, 9);
    blurEffect(inputContrasted, medianBlurred, blurType::median, 3);
    blurEffect(inputContrasted, bilateralBlurred, blurType::bilateral, 60);

    if (showBlurStep) {
        imshow("Gassian 9", gaussianBlurred);
        imshow("Mean 9", meanBlurred);
        imshow("Median 9", medianBlurred);
        imshow("Bilateral 60", bilateralBlurred);

        if (saveResults) {
            imwrite("./images/1-gaussianBlurred.jpg", gaussianBlurred);
            imwrite("./images/1-meanBlurred.jpg", meanBlurred);
            imwrite("./images/1-medianBlurred.jpg", medianBlurred);
            imwrite("./images/1-bilateralBlurred.jpg", bilateralBlurred);
        }
    }

    // Template matching
    if (showTemplateStep) {
        Mat templMatch;
        Mat templ = imread("singleWindow.png", IMREAD_GRAYSCALE);
        templateMatching(input, templ, templMatch, 0, true);
        imshow("Template matching", templMatch);
    }

    // Edge detection
    Mat medianSobelED, medianScharrED, medianLaplacianED, selectedDoubleED;

    edgeDetection(medianBlurred, medianSobelED, kernelType::sobel, blurType::unset);
    edgeDetection(medianBlurred, medianScharrED, kernelType::scharr, blurType::unset);
    edgeDetection(inputContrasted, medianLaplacianED, kernelType::laplacianWithDiagonals, blurType::mean, 3);
    experimentalEdgeDetection(inputContrasted, selectedDoubleED, 0, kernelGenerationType::doubleEdge, kernelType::laplacianWithDiagonals, blurType::mean, 3);

    if (showEdgeStep) {
        imshow("Normal Sobel kernel ED (Median 9)", medianSobelED);
        imshow("Normal Scharr kernel ED (Median 9)", medianScharrED);
        imshow("Normal Laplacian kernel ED (Median 9)", medianLaplacianED);
        imshow("DoubleED Laplacian kernel ED (Median 9)", selectedDoubleED);

        if (saveResults) {
            imwrite("./images/2-medianSobelED.jpg", medianSobelED);
            imwrite("./images/2-medianScharrED.jpg", medianScharrED);
            imwrite("./images/2-medianLaplacianED.jpg", medianLaplacianED);
            imwrite("./images/2-selectedDoubleED.jpg", selectedDoubleED); 
        }
    }

    if (showEdgeStep) edgeDetectionBruteforce(medianBlurred, 6, 1, kernelGenerationType::doubleEdge, kernelType::sobel, blurType::unset, 0, 2, "Double edge detection bruteforce test", "./images/3-DoubleEdgeBruteforce.jpg");
    if (showEdgeStep) edgeDetectionBruteforce(medianBlurred, 6, 1, kernelGenerationType::thickEdge, kernelType::sobel, blurType::unset, 0, 2, "Thick edge detection bruteforce test", "./images/3-ThickEdgeBruteforce.jpg");

    // Threshold
    Mat binaryThreshold, otsuThreshold;

    thresholding(selectedDoubleED, binaryThreshold, thresholdType::binary);
    thresholding(selectedDoubleED, otsuThreshold, thresholdType::otsu);

    if (showThresholdStep) {
        imshow("Binary Threshold", binaryThreshold);
        imshow("Otsu's Threshold", otsuThreshold);

        if (saveResults) {
            imwrite("./images/4-binaryThreshold.jpg", binaryThreshold);
            imwrite("./images/4-otsuThreshold.jpg", otsuThreshold);
        }
    }

    // Morphology
    Mat morphOpen, morphClose;

    morphology(binaryThreshold, morphOpen, morphologyType::isolateLines, morphologyShape::rect, 35, 1);

    morphology(morphOpen, morphClose, morphologyType::extend, morphologyShape::rect, 10, 1);

    if (showMorphStep) {
        imshow("Close operation", morphClose);
        imshow("Open operation", morphOpen);

        if (saveResults) {
            imwrite("./images/5-closeMorph.jpg", morphClose);
            imwrite("./images/5-openMorph.jpg", morphOpen);
        }
    }

    // Detection of windows and or rectangles
    Mat detection;
    std::vector<RotatedRect> rects;

    findRectContours(morphClose, detection, rects, 3000, 20, 2, 1, 10000);

    if (showBoundingBoxStep) {
        imshow("Rect Contours", detection);

        if (saveResults) {
            imwrite("./images/6-rectContours.jpg", detection);
        }
    }

    if (saveResults) {
        std::vector<Mat> images = exportRectBoundingBoxes(inputContrasted, rects);
        exportImages(images);
    }

    waitKey(0);
}