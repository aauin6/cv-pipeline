#include "knnClassifier.hpp"
#include "imageProcessing.hpp"
#include "csvReader.hpp"

using namespace cv;

void featureCalculatorForWindowBlobsTest(std::string vfn, std::string lfn) {
	std::vector<frameData> frameLabels = readAllFiles(2, lfn);
	std::vector<windowBlobs> blobs = correlateFrames(vfn, frameLabels);
    videoFeatures features = featureCalculatorForWindowBlobs(blobs);
}

void correlateFramesTest(std::string vfn, std::string lfn) {
	std::vector<frameData> frameLabels = readAllFiles(2, lfn);
	std::vector<windowBlobs> blobs = correlateFrames(vfn, frameLabels);
}

/**
 * lfn = labelFileName.
 * vfn = videoFileName.
 */
void representBlobsWithinLabelsAsVectorsTest(std::string vfn, std::string lfn) {
    std::vector<frameData> frameLabels = readAllFiles(2, lfn);
    frameData label = frameLabels.at(0);
    VideoCapture vid(vfn);
    Mat frame;
    vid >> frame;
    windowBlobs blobs = representBlobsWithinLabelsAsVector(frame, label);

    // This is visualisation code for the labels
    std::cout << "\nClosed windows:\n";
    for (auto& window : label.closedWindows) {
        std::cout << "Up-x: " << window.upperLeftCornerX << ", Up-y: " << window.upperLeftCornerY << " - " << "Lo-x:" << window.lowerRightCornerX << ", Lo-y:" << window.lowerRightCornerY  << "\n";
	Point upperLeft = Point(window.upperLeftCornerX, window.upperLeftCornerY);
	Point lowerRight = Point(window.lowerRightCornerX, window.lowerRightCornerY);
        rectangle(frame, upperLeft, lowerRight, Scalar(0, 255, 0));
    }
    std::cout << "\n\nOpen windows:\n";
    for (auto& window : label.openWindows) {
        std::cout << "Up-x: " << window.upperLeftCornerX << ", Up-y: " << window.upperLeftCornerY << " - " << "Lo-x:" << window.lowerRightCornerX << ", Lo-y:" << window.lowerRightCornerY << "\n";
	Point upperLeft = Point(window.upperLeftCornerX, window.upperLeftCornerY);
	Point lowerRight = Point(window.lowerRightCornerX, window.lowerRightCornerY);
        rectangle(frame, upperLeft, lowerRight, Scalar(255, 0, 0));
    }

    imshow("Frame", frame);
    waitKey(0);
}

void representBlobsAsVectorsTest() {
    int cols = 9, rows = 5;

    // CR
    Mat binary = Mat::zeros(Size(cols, rows), 0);

    // RC
    binary.at<uchar>(1, 1) = 3;
    binary.at<uchar>(2, 1) = 3;
    binary.at<uchar>(3, 1) = 3;

    binary.at<uchar>(3, 3) = 1;
    binary.at<uchar>(4, 3) = 1;
    binary.at<uchar>(4, 4) = 1;

    binary.at<uchar>(0, 3) = 2;
    binary.at<uchar>(0, 4) = 2;
    binary.at<uchar>(1, 4) = 2;

    binary.at<uchar>(2, 6) = 4;
    binary.at<uchar>(2, 7) = 4;
    binary.at<uchar>(2, 8) = 4;

    std::cout << binary << std::endl;

    std::unordered_map<int, std::vector<objectPixel>> hmap = representBlobsAsVectors(binary);
    std::cout << "The coords are: \nRow: " << hmap.at(1).at(0).row << "\nCol: " << hmap.at(1).at(0).col << std::endl;
    std::cout << hmap.at(2).at(0).value << " " << hmap.at(2).at(1).value << " " << hmap.at(2).at(2).value << std::endl;
    std::cout << hmap.at(2).at(2).row << " " << hmap.at(2).at(2).col << std::endl;
}

void knnClassifier_classifyTest() {
    // Test data
    std::vector<float> testPoint;
    testPoint.push_back(1.8f);
    testPoint.push_back(0.8f);
    testPoint.push_back(91.2f);

    // Number of votes on the object type
    int k = 3;
    knnClassifier classifier;
    std::cout << "The class of the testPoint is: " << classifier.classifyTest(testPoint, k) << std::endl;
}

void knnClassifier_VideoTest(std::string vfn, std::string lfn) {
    std::vector<frameData> frameLabels = readAllFiles(776, lfn);
	std::vector<windowBlobs> blobs = correlateFrames(vfn, frameLabels);
    videoFeatures features = featureCalculatorForWindowBlobs(blobs);
    knnClassifier classifier = knnClassifier(features);

    // Fetch facade images
    Mat input = imread("windowFacade.png", IMREAD_GRAYSCALE);
    std::vector<Mat> images = visionPipeline(input);

    int kNearest =5;
    int n = 1;
    for (auto& image : images) {
        std::cout << "==============================\n" << n << "\n" << "==============================\n";
        classifier.classifyWindow(image, kNearest);
        n++;
    }
}

int main() {
    //representBlobsAsVectorsTest();
    //knnClassifier_classifyTest();
    //representBlobsWithinLabelsAsVectorsTest("c:\\Projects\\uni\\in6-project\\knn_data\\walkingAwayFromFacade.avi", "c:\\Projects\\uni\\in6-project\\knn_data\\csvExport");
    //correlateFramesTest("/home/mikkel/Videos/Project/walkingAwayFromFacade.avi", "/home/mikkel/Videos/Project/FinalExports/csvExport");
    //featureCalculatorForWindowBlobsTest("/home/mikkel/Videos/Project/walkingAwayFromFacade.avi", "/home/mikkel/Videos/Project/FinalExports/csvExport");
    //knnClassifier_VideoTest("/home/mikkel/Videos/Project/walkingAwayFromFacade.avi", "/home/mikkel/Videos/Project/FinalExports/csvExport");
    knnClassifier_VideoTest("C:\\Projects\\uni\\in6-project\\knn_data\\walkingAwayFromFacade.avi", "C:\\Projects\\uni\\in6-project\\knn_data\\csvExport");
	return 0;
}
