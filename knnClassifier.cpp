#include "knnClassifier.hpp"
#include "csvReader.hpp"

bool compareDistances(distanceToTestData d1, distanceToTestData d2) { return (d1.dist < d2.dist); }

/**
 * This returns either 0 == windowState::closed
 * or 1 == windowState::open
 */
int knnClassifier::classifyWindow(cv::Mat window, int kNearest) {
    windowFeatures testDataFeatures = testDataProcessor(window);
    testDataFeatures = inputNormalizer(testDataFeatures);

    std::vector<distanceToTestData> distances;
    // Calculate distance to objects
    for(auto& frame : trainingData.videoClosedFeatures) {
        for (auto& closedWindow : frame) {
            distanceToTestData dist;
            dist.objectClass = 0;
            float distance;
            int i=0; 
            for (auto& feature : closedWindow.features) {
                distance += std::pow(feature - testDataFeatures.features.at(i), 2);
                i++;
            }
            distance = sqrt(distance);
            dist.dist = distance;
            distances.push_back(dist);
        }
    }
    for (auto& frame : trainingData.videoOpenFeatures) {
        for (auto& openWindow : frame) {
            distanceToTestData dist;
            dist.objectClass = 1;
            float distance;
            int i=0;
            for (auto& feature : openWindow.features) {
                distance += std::pow(feature - testDataFeatures.features.at(i), 2);
                i++;
            }
            distance = sqrt(distance);
            dist.dist = distance;
            distances.push_back(dist);
        }
    }
    // Sort distances to objects
    std::sort(distances.begin(), distances.end(), compareDistances);
    //for (auto& distance : distances) {
    //    printf("Dist : %f\n", distance.dist);
    //}
    
    // Access the K closest objects and their classes
    int class0 = { 0 }, class1 = { 0 };
    for (int i = 0; i < kNearest; i++) {
        if (distances.at(i).objectClass == 0)
            class0 += 1;
        else if (distances.at(i).objectClass == 1)
            class1 += 1;
    }
    // return the test class based on a majority vote
    std::cout << "The number of class 0 counts is: " << class0 << "\n" << "The number of class 1 counts is: " << class1 << std::endl;
    if (class0 > class1)
        return 0;
    else if (class1 > class0)
        return 1;
    else {
        std::cerr << "Both classes have equally many votes, result unclear";
        std::cerr << "Please use an uneven K value, to avoid this";
        exit(1);
    }
}

windowFeatures knnClassifier::inputNormalizer(windowFeatures window) {
    printf("Before normalisation: %f, %f\n", window.features.at(0), window.features.at(1));
    std::vector<float> features = window.features;
    std::vector<float> normFeatures;
    int i = 0;
    float xmin, xmax;
    for (auto& feature : features) {
        xmin = minimumValues.at(i); xmax = maximumValues.at(i);
        printf("\nxmin: %f, xmax: %f\n", xmin, xmax);
        printf("feature before: %f\n", feature);
        feature = (feature - xmin) / (xmax - xmin);
        printf("feature after: %f\n\n", feature);
        normFeatures.push_back(feature);
        i++;
    }
    window.features = normFeatures;
    printf("After normalisation: %f, %f\n", window.features.at(0), window.features.at(1));
    return window;
}

// Remember the order in which the features are put into the vector is critical.
// If the order is wrong, then the classification will be wrong.
// The order as of now is: compactness, widthHightRatio
windowFeatures knnClassifier::testDataProcessor(cv::Mat window) {
    windowFeatures output;
    float compactness;
    float widthHeightRatio;
    
    int height = window.rows;
    int width  = window.cols;

    // Check that this is true
    widthHeightRatio = (float) width / height;

    
    int blackPixels = 0, whitePixels = 0;
    for (int i = 0; i < window.rows; i++) {
        for (int j = 0; j < window.cols; j++) {
            if (window.at<uchar>(i, j) == 0)
                blackPixels++;
            else 
                whitePixels++;
        }
    }
    compactness = (float) whitePixels / (blackPixels + whitePixels);

    // printf("The compactness: %f\nThe W/H ratio: %f\n", compactness, widthHeightRatio);
    output.features.push_back(compactness);
    output.features.push_back(widthHeightRatio);
    output.state = windowState::test;
    return output;
}

/**
 * each windowBlobs represents all the blobs of a single frame
 * the vector represents the entire video
 * 
 * PSA: This function needs to be called with binary images.
 * Otherwise it will exit!
 */
videoFeatures featureCalculatorForWindowBlobs(std::vector<windowBlobs> frames) {
    std::vector<std::vector<windowFeatures>> videoClosedFeatures;
    std::vector<std::vector<windowFeatures>> videoOpenFeatures;

	for (auto& frame : frames) {
        std::vector<std::vector<objectPixel>> closedWindows = frame.closedWindowBlobs;
        std::vector<std::vector<objectPixel>> openWindows   = frame.openWindowBlobs;
        int whitePixels;
        int blackPixels;
        std::vector<windowFeatures> closedWindowsFeatures;
        std::vector<windowFeatures> openWindowsFeatures;
        int index = 0;
        for (auto& window : closedWindows) {
            // std::cout << index << " of " << closedWindows.size() << " has been processed" << std::endl;
            windowFeatures currentWindowFeatures;
            currentWindowFeatures.state = windowState::closed;
            whitePixels = 0;
            blackPixels = 0;
            int lowestRow = window.at(0).row;
            int lowestCol = window.at(0).col;
            int highestRow = window.at(0).row;
            int highestCol = window.at(0).col;
            for (auto& pixel : window) {
                if(pixel.value != 0 && pixel.value != 255) {
                    std::cout << "The pixel value is: " << pixel.value << " ";
                }
                if(pixel.row < lowestRow)
                    lowestRow = pixel.row;
                if(pixel.col < lowestCol)
                    lowestCol = pixel.col;
                if(pixel.row > highestRow)
                    highestRow = pixel.row;
                if(pixel.col > highestCol)
                    highestCol = pixel.col;
                if(pixel.value == 255)
                    whitePixels++;
                else if(pixel.value == 0)
                    blackPixels++;
                else {
                    std::cerr << "Non binary image detected, exiting" << std::endl;
                    exit(3);
                } 
                    
            }
            //std::cout << "\nWhitepixels, blackpixels\n" << whitePixels << ", " << blackPixels <<"\n";
            float compactness = (float)whitePixels / (whitePixels + blackPixels);
            //printf("The lowestRow is: %d\nThe lowestCol is: %d\n", lowestRow, lowestCol);
            //printf("The highestRow is: %d\nThe lowestCol is: %d\n\n", highestRow, highestCol);
            // This might be inverted
            int width = highestRow - lowestRow;
            int hight = highestCol - lowestCol;
            float widthToHightRatio = (float) width / hight;

            //std::cout << "Compactness: " << compactness << "\nW/H ratio: " << widthToHightRatio << std::endl;
            currentWindowFeatures.features.push_back(compactness);
            currentWindowFeatures.features.push_back(widthToHightRatio);
            closedWindowsFeatures.push_back(currentWindowFeatures);
            //std::cout << "here" << std::endl;
            index++;
        }

        index = 0;
        for (auto& window : openWindows) {
            //std::cout << index << " of " << closedWindows.size() << " has been processed" << std::endl;
            windowFeatures currentWindowFeatures;
            currentWindowFeatures.state = windowState::open;
            whitePixels = 0;
            blackPixels = 0;
            int lowestRow = -1;
            int lowestCol = -1;
            int highestRow = -1;
            int highestCol = -1;
            for (auto& pixel : window) {
                if (pixel.value != 0 && pixel.value != 255)
                    std::cout << "Pixel value is: " << pixel.value << std::endl;
                if(pixel.row < lowestRow)
                    lowestRow = pixel.row;
                if(pixel.col < lowestCol)
                    lowestCol = pixel.col;
                if(pixel.row > highestRow)
                    highestRow = pixel.row;
                if(pixel.col > highestCol)
                    highestCol = pixel.col;
                if(pixel.value == 255)
                    whitePixels++;
                else if(pixel.value == 0)
                    blackPixels++;
                else {
                    std::cerr << "Non binary image detected, exiting" << std::endl;
                    exit(3);
                }
            }
            float compactness = (float) whitePixels / (whitePixels + blackPixels);

            // This might be inverted
            int width = highestRow - lowestRow;
            int hight = highestCol - lowestCol;
            float widthToHightRatio = (float) width / hight;
            //std::cout << "Compactness: " << compactness << "\nW/H ratio: " << widthToHightRatio << std::endl;
            currentWindowFeatures.features.push_back(compactness);
            currentWindowFeatures.features.push_back(widthToHightRatio);
            openWindowsFeatures.push_back(currentWindowFeatures);
        }
        videoClosedFeatures.push_back(closedWindowsFeatures);
        videoOpenFeatures.push_back(openWindowsFeatures);
	}
	//Iterate through vector.
	//Iterate through windowBlobs
    //Count pixels with values of 255 and 0 (should only be used with binary images, right?)

    videoFeatures res;
    res.videoClosedFeatures = videoClosedFeatures;
    res.videoOpenFeatures = videoOpenFeatures;
    return res;
}

// This is the constructor to use when data has been loaded from a file (or where have you).
knnClassifier::knnClassifier(videoFeatures videoData) {
    //std::cout << "knnClassifier constructor called!" << std::endl;
    trainingNormalizer(videoData);
}

void knnClassifier::trainingNormalizer(videoFeatures videoData) {
    // x' = (x - x_min) / (x_max - x_min)
    // numberOfFrames = videoData.videoClosedFeatures.size();
    // numberOfClosedWindowsInAFrame = videoData.videoClosedFeatures.at(0).size();
    int numberOfFeatures = videoData.videoClosedFeatures.at(0).at(0).features.size();
    
    for (int i = 0; i < numberOfFeatures; i++) {
        maximumValues.push_back(videoData.videoClosedFeatures.at(0).at(0).features.at(i));
        minimumValues.push_back(videoData.videoClosedFeatures.at(0).at(0).features.at(i));
    }
    // Iterate over every frame
    for (int i = 0; i < videoData.videoClosedFeatures.size(); i++) {
        // Iterate over every closed window
        for (int j = 0; j < videoData.videoClosedFeatures.at(i).size(); j++) {
            // Save ram by making these values pointers (They might be unnecessarily copied at the moment)
            std::vector<windowFeatures> windows = videoData.videoClosedFeatures.at(i);
            for (int win = 0; win < windows.size(); win++) {
                std::vector<float> windowFeatures = windows.at(win).features;
                // Iterate over every feature
                for (int k = 0; k < numberOfFeatures; k++) {
                    if (windowFeatures.at(k) < minimumValues.at(k)) {
                        minimumValues.at(k) = windowFeatures.at(k);
                    }
                    else if (windowFeatures.at(k) > maximumValues.at(k)) {
                        maximumValues.at(k) = windowFeatures.at(k);
                    }
                }
            }
        }
        // Iterate over every open window
        for (int j = 0; j < videoData.videoOpenFeatures.at(i).size(); j++) {
            // Save ram by making these values pointers (They might be unnecessarily copied at the moment)
            std::vector<windowFeatures> windows = videoData.videoOpenFeatures.at(i);
            for (int win = 0; win < windows.size(); win++) {
                std::vector<float> windowFeatures = windows.at(win).features;
                // Iterate over every feature
                for (int k = 0; k < numberOfFeatures; k++) {
                    if (windowFeatures.at(k) < minimumValues.at(k)) {
                        minimumValues.at(k) = windowFeatures.at(k);
                    }
                    else if (windowFeatures.at(k) > maximumValues.at(k)) {
                        maximumValues.at(k) = windowFeatures.at(k);
                    }
                }
            }
        }
    }

    float xres, xmin, xmax;

    // Iterate over all features of all closed windows in all frames
    std::vector<std::vector<windowFeatures>>& allFramesClosed = videoData.videoClosedFeatures;
    for (auto& frameClosed : allFramesClosed) {
        for (auto& window : frameClosed) {
            int i = 0;
            for (auto& feature : window.features) {
                xmin = minimumValues.at(i);
                xmax = maximumValues.at(i);
                // This should set the feature value inside the original videoData
                // struct, since it uses pointers all the way back
                feature = (feature - xmin) / (xmax - xmin);
                i++;
            }
        }
    }
    std::vector<std::vector<windowFeatures>> allFramesOpen = videoData.videoOpenFeatures;
    for (auto& frameOpen : allFramesOpen) {
        for (auto& window : frameOpen) {
            int i = 0;
            for (auto& feature : window.features) {
                xmin = minimumValues.at(i);
                xmax = maximumValues.at(i);
                //std::cout << "xmax, xmin\n" << xmax << ", " << xmin << std::endl;
                // This should set the feature value inside the original videoData
                // struct, since it uses pointers all the way back
                //std::cout << "Before training data normalisation: "<< feature << std::endl;
                feature = (feature - xmin) / (xmax - xmin);
                //std::cout << "After training data normalisation: "<< feature << "\n" << std::endl;
                i++;
            }
        }
    }

    trainingData = videoData;
}








//-----------------------------------------------------------------------//
                        // Code for testing //

//-----------------------------------------------------------------------//

// This is meant for testing with preset data.
knnClassifier::knnClassifier() {
    // This might be necessary to ensure that the data is not used without it being normalized first, which would be bad...
    trainingTestData = dataInitializerTest();
    trainingTestDataNormalizer();
}

// This function generates data for the knnTest
std::vector<std::vector<float>> knnClassifier::dataInitializerTest() {

    // This creates the datastructure and fills it in
    std::vector<std::vector<float>> trainingTestData;
    std::vector<float> dataPoint1, dataPoint2, dataPoint3, dataPoint4, dataPoint5, dataPoint6;

    // This data describes a person (just to test the setup)
    dataPoint1.push_back(1.8f); //height in meters
    dataPoint1.push_back(0.5f); //width in meters
    dataPoint1.push_back(73.2f); //weight in kg
    dataPoint1.push_back(1); //class

    dataPoint2.push_back(1.95f); //height in meters
    dataPoint2.push_back(0.55f); //width in meters
    dataPoint2.push_back(96.8f); //weight in kg
    dataPoint2.push_back(1); //class

    dataPoint3.push_back(1.85f); //height in meters
    dataPoint3.push_back(0.60f); //width in meters
    dataPoint3.push_back(86.8f); //weight in kg
    dataPoint3.push_back(1); //class

    dataPoint4.push_back(1.6f); //height in meters
    dataPoint4.push_back(0.45f); //width in meters
    dataPoint4.push_back(62.4f); //weight in kg
    dataPoint4.push_back(2); //class

    dataPoint5.push_back(1.55f); //height in meters
    dataPoint5.push_back(0.40f); //width in meters
    dataPoint5.push_back(48.2f); //weight in kg
    dataPoint5.push_back(2); //class

    dataPoint6.push_back(1.6f); //height in meters
    dataPoint6.push_back(0.50f); //width in meters
    dataPoint6.push_back(62.4f); //weight in kg
    dataPoint6.push_back(2); //class

    trainingTestData.push_back(dataPoint1);
    trainingTestData.push_back(dataPoint2);
    trainingTestData.push_back(dataPoint3);
    trainingTestData.push_back(dataPoint4);
    trainingTestData.push_back(dataPoint5);
    trainingTestData.push_back(dataPoint6);

    return trainingTestData;
}

void knnClassifier::trainingTestDataNormalizer() {
    // x' = (x - x_min) / (x_max - x_min)

    // We subtract one from this number, since the label is not something to be normalized
    int numberOfFeatures = (int)trainingTestData.at(0).size() - 1;

    // EDIT: Legacy comments below:
    // These allocations (using new and delete) are needed because the size is not known at compile time. 
    // Therefore, to make it compile, we have to handle the allocation/deallocation manually, hence the deletes at the bottom of the function.
    // EDIT: Since the values are saved by the pointer in the object, the deletes have been removed, but they would have been neccesary, if the function did not need to save them anywhere.


    for (int i = 0; i < numberOfFeatures; i++) {
        maximumValues.push_back(trainingTestData.at(0).at(i));
        minimumValues.push_back(trainingTestData.at(0).at(i));
    }

    for (int i = 1; i < trainingTestData.size(); i++) {
        for (int j = 0; j < numberOfFeatures; j++) {
            if (trainingTestData.at(i).at(j) < minimumValues.at(j)) {
                minimumValues.at(j) = trainingTestData.at(i).at(j);
            }
            else if (trainingTestData.at(i).at(j) > maximumValues.at(j)) {
                maximumValues.at(j) = trainingTestData.at(i).at(j);
            }
        }
    }

    float xres, x, xmin, xmax;

    for (int i = 0; i < trainingTestData.size(); i++) {
        /* Debug statement below
        std::cout << "Training data number " << i << std::endl;
        std::cout << std::endl;
        */
        for (int j = 0; j < numberOfFeatures; j++) {
            xmin = minimumValues.at(j);
            xmax = maximumValues.at(j);
            x = trainingTestData.at(i).at(j);
            xres = (x - xmin) / (xmax - xmin);
            trainingTestData.at(i).at(j) = xres;
            // Debug statement below
            // std::cout << "X min: " << xmin << " X max: " << xmax << " X: " << x << " X result: " << xres << std::endl;
        }
    }
    // find the maximum value of each feature as well as the minimum value of each feature.
    // apply the formula to normalize all the feature values.
}

std::vector<float> knnClassifier::inputNormalizerTest(std::vector<float> input) {
    float xmax;
    float xmin;
    float x;
    float xres;

    for (int i = 0; i < input.size(); i++) {
        xmax = maximumValues.at(i);
        xmin = minimumValues.at(i);
        x = input.at(i);
        xres = (x - xmin) / (xmax - xmin);
        input.at(i) = xres;
    }

    return input;
}

int knnClassifier::classifyTest(std::vector<float> testPoint, int k) {

    std::vector<float> testPointNorm = inputNormalizerTest(testPoint);

    // Remember: The dimension of testPoint is NOT the same as the trainingTestData... The trainingTestData is labled, and the label is at the very end of the vector. End of PSA :)

    // Choosing the number of data point for the comparison
    // Below is a link describing how to make features for KNN equally important (specifically focus on "normalization"
    // https://www.analyticsvidhya.com/blog/2020/04/feature-scaling-machine-learning-normalization-standardization/

    std::vector<distanceToTestData> distances;
    for (auto& data : trainingTestData) {
        float dist = 0;
        for (int i = 0; i < testPointNorm.size(); i++) {
            dist += std::pow(data.at(i) - testPointNorm.at(i), 2);
        }
        dist = sqrt(dist);
        distanceToTestData distToObject = { dist, (int)data.back() };
        distances.push_back(distToObject);
    }
    /// Add comparison function:
    std::sort(distances.begin(), distances.end(), compareDistances);
    /* Debug statement below
    std::cout << "The distances between the testPoint and the 6 different pieces of trainingTestData" << std::endl;
    std::cout << distances.at(0).dist << std::endl;
    std::cout << distances.at(1).dist << std::endl;
    std::cout << distances.at(2).dist << std::endl;
    std::cout << distances.at(3).dist << std::endl;
    std::cout << distances.at(4).dist << std::endl;
    std::cout << distances.at(5).dist << std::endl;
    */
    // Idea: instead of simply saving the distances to the vector, maybe one could use a heap to sort the data in a tree structure. This might make it run faster in general. End of PSA :)

    int class1 = { 0 }, class2 = { 0 };
    for (int i = 0; i < k; i++) {
        if (distances.at(i).objectClass == 1)
            class1 += 1;
        else if (distances.at(i).objectClass == 2)
            class2 += 1;
    }
    /* Debug statement below
    std::cout << "The number of class 1 counts is: " << class1 << "\n" << "The number of class 2 counts is: " << class2 << std::endl;
    */
    if (class1 > class2)
        return 1;
    else if (class2 > class1)
        return 2;
    else {
        std::cerr << "Both classes have equally many votes, result unclear";
        std::cerr << "Please use an uneven K value, to avoid this";
        exit(1);
    }
}
